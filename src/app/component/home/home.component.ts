import { Component, OnInit } from '@angular/core';

import { Description, List } from 'src/app/shared/interface.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  flag: boolean = false;
  date: any = new Date().toUTCString().slice(5, 12);

  displayedColumns: string[] = ['date', 'listName', 'noOfEntities', 'actions', 'details'];
  displayedDescriptionColumns: string[] = ['detailDescription' ];
  
  description: Description[] = [
    {detailDescription: 'Puma Sports India Pvt Ltd'},
    {detailDescription: 'Infosys'},
    {detailDescription: 'LTIMindtree'},
    {detailDescription: 'Think & Learn Pvt Ltd'},
    {detailDescription: 'Cooler'},
    {detailDescription: 'Swim suit'}
  ];

  ELEMENT_DATA: List[] = [
    {date: 1, listName: 'Competitve Intelligence', noOfEntities: 1, actions: ['email'], details: 'Details'},
    {date: 1, listName: 'My Vendors', noOfEntities: 1, actions: ['email'], details: 'Details'},
    {date: 1, listName: 'My Customer', noOfEntities: 1, actions: ['email'], details: 'Details'},
    {date: 1, listName: 'Hydrogen', noOfEntities: 1, actions: ['email'], details: 'Details'},
    {date: this.date, listName: 'Test_30_results', noOfEntities: 1, actions: ['email', 'share', 'edit', 'delete'], details: 'Details'},
    {date: this.date, listName: 'To_index', noOfEntities: 1, actions: ['email', 'share', 'edit', 'delete'], details: 'Details'},
    {date: this.date, listName: 'KPMG interested', noOfEntities: 1, actions: ['email', 'share', 'edit', 'delete'], details: 'Details'},
    {date: this.date, listName: 'Two companies', noOfEntities: 1, actions: ['email', 'share', 'edit', 'delete'], details: 'Details'},
    {date: this.date, listName: 'custom', noOfEntities: 1, actions: ['email', 'share', 'edit', 'delete'], details: 'Details'},
    {date: this.date, listName: 'mumbai', noOfEntities: 1, actions: ['email', 'share', 'edit', 'delete'], details: 'Details'},
    {date: this.date, listName: 'Error case- LTB to EBIDITA Blank', noOfEntities: 1, actions: ['email', 'share', 'edit', 'delete'], details: 'Details'},
    {date: this.date, listName: 'Hydrogen', noOfEntities: 1, actions: ['email', 'share', 'edit', 'delete'], details: 'Details'},
    {date: this.date, listName: 'Hydrogen', noOfEntities: 1, actions: ['email', 'share', 'edit', 'delete'], details: 'Details'},
    {date: this.date, listName: 'Hydrogen', noOfEntities: 1, actions: ['email', 'share', 'edit', 'delete'], details: 'Details'},
  ];

  dataSource = this.ELEMENT_DATA;

  
  constructor() { }

  ngOnInit(): void {
    this.date = new Date().toUTCString().slice(5, 12);
    console.log(this.date);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
}

showDescription() {
  this.flag = this.flag ? false : true;
  console.log("ankit");
}

isNumber(val: any): boolean { return typeof val === 'number'; }

}
