export interface List {
    date: string | number;
    listName: string;
    noOfEntities: number;
    actions: string[];
    details: string;
}

export interface Description {
    detailDescription: string;
}